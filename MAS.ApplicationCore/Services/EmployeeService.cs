﻿using MAS.ApplicationCore.DTOs;
using MAS.ApplicationCore.Entities;
using MAS.ApplicationCore.Factories;
using MAS.ApplicationCore.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MAS.ApplicationCore.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<IEnumerable<EmployeeDTO>> GetAll()
        {
            var employeeDTOList = new List<EmployeeDTO>();

            var employees = await _employeeRepository.List();

            foreach (var employee in employees)
            {
                var employeeDTO = EmployeeDTOFactory.GetEmployeeDTO(employee);
                employeeDTOList.Add(employeeDTO);
            }

            return employeeDTOList;
        }

        public async Task<EmployeeDTO> GetById(int employeeId)
        {
            var employee = await _employeeRepository.GetById(employeeId);

            var employeeDTO = EmployeeDTOFactory.GetEmployeeDTO(employee);

            return employeeDTO;
        }
    }
}