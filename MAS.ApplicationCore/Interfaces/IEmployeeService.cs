﻿using MAS.ApplicationCore.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MAS.ApplicationCore.Interfaces
{
    public interface IEmployeeService
    {
        Task<EmployeeDTO> GetById(int employeeId);
        Task<IEnumerable<EmployeeDTO>> GetAll();
    }
}