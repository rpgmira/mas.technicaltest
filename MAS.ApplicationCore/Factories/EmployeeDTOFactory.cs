﻿using AutoMapper;
using MAS.ApplicationCore.DTOs;
using MAS.ApplicationCore.Entities;
using System;

namespace MAS.ApplicationCore.Factories
{
    internal static class EmployeeDTOFactory
    {
        private static IMapper _mapper;
        private static IMapper Mapper
        {
            get
            {
                if (_mapper == null)
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Employee, HourlySalaryEmployeeDTO>();
                        cfg.CreateMap<Employee, MonthlySalaryEmployeeDTO>();
                    });
                    _mapper = config.CreateMapper();
                }
                return _mapper;
            }
        }

        internal static EmployeeDTO GetEmployeeDTO(Employee employee)
        {
            if (employee == null)
                return null;

            EmployeeDTO employeeDTO;

            switch (employee.ContractTypeName)
            {
                case "HourlySalaryEmployee":
                    employeeDTO = Mapper.Map<Employee, HourlySalaryEmployeeDTO>(employee);
                    break;
                case "MonthlySalaryEmployee":
                    employeeDTO = Mapper.Map<Employee, MonthlySalaryEmployeeDTO>(employee);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("ContractTypeName", "ContractTypeName for the employee not recognized.");
            }

            return employeeDTO;
        }
    }
}