﻿namespace MAS.ApplicationCore.DTOs
{
    public class MonthlySalaryEmployeeDTO : EmployeeDTO
    {
        public override decimal AnnualSalary { get { return MonthlySalary * 12; } }
    }
}