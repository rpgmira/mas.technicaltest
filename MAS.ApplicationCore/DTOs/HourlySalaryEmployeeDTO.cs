﻿namespace MAS.ApplicationCore.DTOs
{
    public class HourlySalaryEmployeeDTO : EmployeeDTO
    {
        public override decimal AnnualSalary { get { return 120 * HourlySalary * 12; } }
    }
}