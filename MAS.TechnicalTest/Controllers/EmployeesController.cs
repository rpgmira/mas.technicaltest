﻿using MAS.ApplicationCore.DTOs;
using MAS.ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MAS.TechnicalTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        // GET: api/Employee
        [HttpGet]
        public async Task<IEnumerable<EmployeeDTO>> Get()
        {
            var employees = await GetEmployees();
            return employees;
        }

        // GET: api/Employee/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IEnumerable<EmployeeDTO>> Get(string id)
        {
            var employees = await GetEmployees(id);
            return employees;
        }

        private async Task<IEnumerable<EmployeeDTO>> GetEmployees(string id = null)
        {
            if (string.IsNullOrWhiteSpace(id))
                return await _employeeService.GetAll();
            else if (int.TryParse(id, out int employeeId))
            {
                var employee = await _employeeService.GetById(employeeId);
                if (employee != null)
                    return new EmployeeDTO[] { employee };
            }

            return new EmployeeDTO[] {};
        }
    }
}