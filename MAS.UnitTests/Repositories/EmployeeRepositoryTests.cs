﻿using MAS.ApplicationCore.Entities;
using MAS.ApplicationCore.Interfaces;
using MAS.Infrastructure.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace MAS.UnitTests.Repositories
{
    [TestClass]
    public class EmployeeRepositoryTests
    {
        // Unit test naming convention:
        // [UnitOfWork_StateUnderTest_ExpectedBehavior]

        private IRepository<Employee> employeeRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            employeeRepository = new EmployeeRepository();
        }

        [TestCleanup]
        public void TestCleanup()
        {
        }

        [TestMethod]
        public async Task List_All_RetrievesAllEmployees()
        {
            var actual = await employeeRepository.List();

            Assert.IsTrue(actual.Any());
        }

        [TestMethod]
        public async Task GetById_1_RetrievesJuanInformation()
        {
            var employeeId = 1;

            var expected = new Employee
            {
                Id = employeeId,
                Name = "Juan",
                ContractTypeName = "HourlySalaryEmployee",
                RoleId = 1,
                RoleName = "Administrator",
                RoleDescription = null,
                HourlySalary = 60000,
                MonthlySalary = 80000
            };

            var actual = await employeeRepository.GetById(employeeId);

            AssertAreEqual(expected, actual);
        }

        [TestMethod]
        public async Task GetById_2_RetrievesSebastianInformation()
        {
            var employeeId = 2;

            var expected = new Employee
            {
                Id = employeeId,
                Name = "Sebastian",
                ContractTypeName = "MonthlySalaryEmployee",
                RoleId = 2,
                RoleName = "Contractor",
                RoleDescription = null,
                HourlySalary = 60000,
                MonthlySalary = 80000
            };

            var actual = await employeeRepository.GetById(employeeId);

            AssertAreEqual(expected, actual);
        }

        [TestMethod]
        public async Task GetById_3_RetrievesNull()
        {
            var employeeId = 3;

            var actual = await employeeRepository.GetById(employeeId);

            Assert.IsNull(actual);
        }

        #region Private methods

        private void AssertAreEqual(Employee expected, Employee actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.ContractTypeName, actual.ContractTypeName);
            Assert.AreEqual(expected.RoleId, actual.RoleId);
            Assert.AreEqual(expected.RoleName, actual.RoleName);
            Assert.AreEqual(expected.RoleDescription, actual.RoleDescription);
            Assert.AreEqual(expected.HourlySalary, actual.HourlySalary);
            Assert.AreEqual(expected.MonthlySalary, actual.MonthlySalary);
        }

        #endregion Private methods
    }
}