﻿using MAS.ApplicationCore.DTOs;
using MAS.ApplicationCore.Entities;
using MAS.ApplicationCore.Interfaces;
using MAS.ApplicationCore.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAS.UnitTests.Services
{
    [TestClass]
    public class EmployeeServiceTests
    {
        #region Mocked data

        private readonly Employee mockedEmployee4 = new Employee
        {
            Id = 4,
            Name = "Pedro",
            ContractTypeName = "HourlySalaryEmployee",
            RoleId = 1,
            RoleName = "Administrator",
            RoleDescription = null,
            HourlySalary = 30000,
            MonthlySalary = 40000
        };

        private readonly Employee mockedEmployee5 = new Employee
        {
            Id = 5,
            Name = "Pablo",
            ContractTypeName = "MonthlySalaryEmployee",
            RoleId = 2,
            RoleName = "Contractor",
            RoleDescription = null,
            HourlySalary = 30000,
            MonthlySalary = 40000
        };

        private readonly Employee mockedEmployee6 = new Employee
        {
            Id = 6,
            Name = "Luis",
            ContractTypeName = "QuarterlySalaryEmployee",
            RoleId = 3,
            RoleName = "Shareholder",
            RoleDescription = null,
            HourlySalary = 30000,
            MonthlySalary = 40000
        };

        #endregion Mocked data

        private IEmployeeService employeeService;
        private IRepository<Employee> employeeRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            var employeeRepositoryMock = new Mock<IRepository<Employee>>();
            employeeRepositoryMock
                .Setup(x => x.GetById(4))
                .Returns(MockRepositoryGetById(4));
            employeeRepositoryMock
                .Setup(x => x.GetById(5))
                .Returns(MockRepositoryGetById(5));
            employeeRepositoryMock
                .Setup(x => x.GetById(6))
                .Returns(MockRepositoryGetById(6));
            employeeRepositoryMock
               .Setup(x => x.GetById(7))
               .Returns(MockRepositoryGetById(7));
            employeeRepositoryMock
                .Setup(x => x.List())
                .Returns(MockRepositoryList());
            employeeRepository = employeeRepositoryMock.Object;
            employeeService = new EmployeeService(employeeRepository);            
        }

        [TestCleanup]
        public void TestCleanup()
        {
        }

        [TestMethod]
        public async Task GetAll_All_RetrievesAllMockedEmployees()
        {
            var actual = await employeeService.GetAll();

            Assert.IsTrue(actual.Any());
        }

        [TestMethod]
        public async Task GetById_4_RetrievesMockedPedroInformation()
        {
            var employeeId = 4;

            var expected = new HourlySalaryEmployeeDTO
            {
                Id = employeeId,
                Name = "Pedro",
                ContractTypeName = "HourlySalaryEmployee",
                RoleId = 1,
                RoleName = "Administrator",
                RoleDescription = null,
                HourlySalary = 30000,
                MonthlySalary = 40000
            };
            var expectedAnnualSalary = 120 * expected.HourlySalary * 12;

            var actual = await employeeService.GetById(employeeId);

            Assert.AreEqual(expected.GetType(), typeof(HourlySalaryEmployeeDTO));
            Assert.AreEqual(expectedAnnualSalary, actual.AnnualSalary);
            AssertAreEqual(expected, actual);
        }

        [TestMethod]
        public async Task GetById_5_RetrievesMockedPabloInformation()
        {
            var employeeId = 5;

            var expected = new MonthlySalaryEmployeeDTO
            {
                Id = employeeId,
                Name = "Pablo",
                ContractTypeName = "MonthlySalaryEmployee",
                RoleId = 2,
                RoleName = "Contractor",
                RoleDescription = null,
                HourlySalary = 30000,
                MonthlySalary = 40000
            };
            var expectedAnnualSalary = expected.MonthlySalary * 12;

            var actual = await employeeService.GetById(employeeId);

            Assert.AreEqual(expected.GetType(), typeof(MonthlySalaryEmployeeDTO));
            Assert.AreEqual(expectedAnnualSalary, actual.AnnualSalary);
            AssertAreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public async Task GetById_6_ThrowsArgumentOutOfRangeException()
        {
            var employeeId = 6;            

            var actual = await employeeService.GetById(employeeId);
        }

        [TestMethod]
        public async Task GetById_7_RetrievesNull()
        {
            var employeeId = 7;

            var actual = await employeeService.GetById(employeeId);

            Assert.IsNull(actual);
        }

        #region Private methods

        private Task<Employee> MockRepositoryGetById(int employeeId)
        {
            Employee employee;

            switch (employeeId)
            {
                case 4:
                    employee = mockedEmployee4;
                    break;
                case 5:
                    employee = mockedEmployee5;
                    break;
                case 6:
                    employee = mockedEmployee6;
                    break;
                default:
                    employee = null;
                    break;
            }
            return Task.FromResult(employee);
        }        

        private Task<IEnumerable<Employee>> MockRepositoryList()
        {
            IEnumerable<Employee> employees = new List<Employee>() { mockedEmployee4, mockedEmployee5 };
            return Task.FromResult(employees);
        }

        private void AssertAreEqual(EmployeeDTO expected, EmployeeDTO actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.ContractTypeName, actual.ContractTypeName);
            Assert.AreEqual(expected.RoleId, actual.RoleId);
            Assert.AreEqual(expected.RoleName, actual.RoleName);
            Assert.AreEqual(expected.RoleDescription, actual.RoleDescription);
            Assert.AreEqual(expected.HourlySalary, actual.HourlySalary);
            Assert.AreEqual(expected.MonthlySalary, actual.MonthlySalary);            
        }

        #endregion Private methods
    }
}