# Technical Test for MAS Global Consulting

Technical Test for MAS Global Consulting in ASP.NET Core by Juan Felipe Mira Uribe

## Tools

* Visual Studio 2019
* .NET Core 2.1

## How To Run

* Open solution in Visual Studio 2019
* Restore NuGet packages
* Set MAS.TechnicalTest project as Startup Project
* Build the project.
* Run the application.