﻿using MAS.ApplicationCore.Entities;
using MAS.ApplicationCore.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;

namespace MAS.Infrastructure.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private readonly HttpClient HttpClient;        

        public EmployeeRepository()
        {
            HttpClient = new HttpClient() {
                BaseAddress = new Uri("http://masglobaltestapi.azurewebsites.net/api/")
            };
        }

        public Task Add(Employee entity)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Employee entity)
        {
            throw new NotImplementedException();
        }

        public Task Edit(Employee entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Employee> GetById(int id)
        {
            var employeeList = await List();
            var result = employeeList.SingleOrDefault(x => x.Id == id);

            return result;
        }

        public async Task<IEnumerable<Employee>> List()
        {
            var query = "Employees";
            var response = await HttpClient.GetAsync(query);
            var content = await response.Content.ReadAsStringAsync();
            var jArray = JArray.Parse(content);
            var result = jArray.ToObject<IEnumerable<Employee>>();

            return result;
        }

        public Task<IEnumerable<Employee>> List(Expression<Func<Employee, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}